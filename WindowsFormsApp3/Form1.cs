﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        public Form1()
        {
            InitializeComponent();
        }

        private void displayResult(String element)
        {
            label.Text += element+" ";
        }
        private void clearInput()
        {
            textBox.Text = "";
        }

        private void clearLabel()
        {
            label.Text = "";
        }

        private void clearData(String flage = "")
        {
            if(flage != "toPreapare")
            {
                data.Remove("number1");
            }
            data.Remove("number2");
            data.Remove("operation");
            data.Remove("result");
        }
        private void setNumbers(string key)
        {
            data.Add(key, textBox.Text);
        }

        private void setOperations(String operation)
        {
            data.Add("operation", operation);
        }

        private void calculate()
        {
            float result = 0;
            float n1 = float.Parse(data["number1"]);
            Console.WriteLine("t2 " + data["number2"]);
            float n2 = float.Parse(data["number2"]);
            switch (data["operation"])
            {
                case "+":
                    result = n1 + n2;
                    break;
                case "-":
                    result = n1 - n2;
                    break;
                case "*":
                    result = n1 * n2;
                    break;
                case "/":
                    result = n1 / n2;
                    break;
            }
            data.Add("result", result.ToString());
        }

        private void clear(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            clearInput();
            clearLabel();
            clearData();
        }
        private void preapareToNextCalculation()
        {
            clearInput();
            clearLabel();
            data["number1"] = data["result"];
            clearData("toPreapare");
        }
        private void buttonClick(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (data.ContainsKey("number1") && data.ContainsKey("operation") && !data.ContainsKey("number2"))
            {
                setNumbers("number2");
                displayResult(data["number2"]);
            }
            if (!data.ContainsKey("number1") && !data.ContainsKey("number2"))
            {
                setNumbers("number1");
                displayResult(data["number1"]);
            }
            if (!data.ContainsKey("operation") && b.Text != "" && b.Text != "=")
            {
                setOperations(b.Text);
                displayResult(data["operation"]);
            }
            if (data.ContainsKey("number1") && data.ContainsKey("number2") && data.ContainsKey("operation"))
            {
                calculate();
                preapareToNextCalculation();
                displayResult(data["number1"]);
            }
            clearInput();
        }
    }
}